﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour
{
    public AudioSource playerAS;
    public AudioClip pickup;
    public AudioClip hurt;
    public AudioClip powerup;
    public AudioClip gameOver;
    [Range(0.1f, 1.0f)] [SerializeField] float pickSoundVolume;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void playPickup()
    {
        playerAS.PlayOneShot(pickup, pickSoundVolume);
    }
    public void playHurt()
    {
        playerAS.PlayOneShot(hurt, pickSoundVolume);
    }
    public void playPowerup()
    {
        playerAS.PlayOneShot(powerup, pickSoundVolume);
    }
    public void playGameOver()
    {
        playerAS.PlayOneShot(gameOver, pickSoundVolume);
    }

}
