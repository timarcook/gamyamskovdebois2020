﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.UI;


public class ScoreSystem : MonoBehaviour
{
    public static ScoreSystem instance;

    [SerializeField] Text scoreText;

    [Header("Events")]
    public UnityEvent onAddScore;
    public UnityEvent<int> onAddScoreIntPara;
    
    int score = 0;

    // Use this for initialization
    void Start()
    {
        scoreText.text = "Score: " + this.score;
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void AddScore(int score)
    {
        this.score += score;
        onAddScore.Invoke();
        onAddScoreIntPara.Invoke(this.score);
        scoreText.text = "Score: " + this.score;
    }

    public int getScore()
    {
        return score;
    }
}
