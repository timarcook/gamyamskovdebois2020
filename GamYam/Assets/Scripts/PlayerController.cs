﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Rigidbody2D rb2d;
    private Vector2 moveVector;
    [Range(1f, 200f)] [SerializeField] float moveSpeed = 1;
    [Range(1f, 30f)] [SerializeField] float moveSpeedMultiplier = 1;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        movePlayer();
    }
    void movePlayer()
    {
        float moveVertical = Input.GetAxis("Vertical");
        float moveHorizontal = Input.GetAxis("Horizontal");
        moveVector = new Vector2(moveHorizontal, moveVertical);
        rb2d.AddForce(moveVector * (moveSpeed * moveSpeedMultiplier)* Time.deltaTime);

    }
}
