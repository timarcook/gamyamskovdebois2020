﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    ParticleSystem particleSystem;
    private void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }
    public void Init(Color color, Vector3 pos)
    {
        var main = particleSystem.main;
        main.startColor = color;
        this.transform.position = pos;
        particleSystem.Play();
    }



}
