﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour
{
    public static float speed = 3.0f;
    private float gameSpeed = 2.0f;
    private Rigidbody2D rb;
    private Vector2 screenBounds;
    public SpriteRenderer spriteRenderer;
    public List<Sprite> list;
    private int randomSprite;
    public Color32 color;
    private Color32 Yellow = new Color32(249, 251, 63, 255);
    private Color32 Pink = new Color32(255, 0, 177, 255);
    private Color32 White = new Color32(255, 255, 255, 255);
    private Color32 Teal = new Color32(0, 255, 206, 255);

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        ChangeSprite();

        color = spriteRenderer.color;
        changeColor();

        StartCoroutine(SpeedOnObject());
        if (speed > 10)
        {
            gameSpeed = +Random.Range(1, 15);
            Debug.Log("START >10");
        }
        if (speed > 5 && speed < 10)
            gameSpeed = speed + Random.Range(-3, 5);
        if (speed < 5)
            gameSpeed = speed;
        float verticality = Random.Range(-4, 4);
        rb = this.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(-gameSpeed, verticality);
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

    }

    void changeColor() 
    {
        Random rand = new Random();
        int i = Random.Range(0, 4);
        if (i == 1)
            color = Yellow;
        if (i == 2)
            color = Pink;
        if (i == 3)
            color = White;
        if (i == 0)
            color = Teal;
        spriteRenderer.color = color;
    }

    void ChangeSprite()
    {
        randomSprite = (int) Random.Range(0, list.Count);
        spriteRenderer.sprite = list[randomSprite];
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < -screenBounds.x * 2)
        {
            Destroy(this.gameObject);
        }
    }
    IEnumerator SpeedOnObject()
    {
         
            if (speed < 15)
            {
                Debug.Log("Speed has changed: "+speed);
                speed += (float)0.05;
                yield return new WaitForSeconds(1);
            }

           
    }
}
