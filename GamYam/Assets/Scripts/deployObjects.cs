﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deployObjects : MonoBehaviour
{
    public GameObject objectPrefab;
    public float respawnTime = 1.0f;
    public float scoreCounter = 1;
    private Vector2 screenBounds;

    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height-150, Camera.main.transform.position.z));
        Debug.Log("Screen height: " + Screen.height);
        StartCoroutine(objectWave());
    }

    private void spawnObject()
    {
        GameObject a = Instantiate(objectPrefab) as GameObject;
        a.transform.position = new Vector2(10, Random.Range(-screenBounds.y, screenBounds.y));
    }
    
    IEnumerator objectWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            scoreCounter = (ScoreSystem.instance.getScore() / 750) + 1;
            for (int i = 0; i < scoreCounter; i++) 
            {
                spawnObject();
            }

        }
    }
}
