﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;


public class ColorBehaviour : MonoBehaviour
{
    SpriteRenderer paintElefant;
    public Color32 color;
    public int changeColorTimer = 4;
    private Color32 Yellow = new Color32(249, 251, 63, 255);
    private Color32 Pink = new Color32(255, 0, 177, 255);
    private Color32 White = new Color32(255, 255, 255, 255);
    private Color32 Teal = new Color32(0, 255, 206, 255);
    // Start is called before the first frame update
    void Start()
    {
        paintElefant = GetComponent<SpriteRenderer>();
        color = paintElefant.color;
        paintElefant.color = color;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void changeColorFunction()
    {
        Random rand = new Random();
        int i = Random.Range(0, 4);
        if (i == 1)
            color = Yellow;
        if (i == 2)
            color = Pink;
        if (i == 3)
            color = White;
        if (i == 0)
            color = Teal;
        paintElefant.color = color;
    }
}
