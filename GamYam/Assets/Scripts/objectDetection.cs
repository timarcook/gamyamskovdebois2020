﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectDetection : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Objects"))
        {
            Object objectCollision = collision.gameObject.GetComponent<Object>();
            ColorBehaviour ColorBehaviour = gameObject.GetComponent<ColorBehaviour>();

            if (ColorBehaviour.color.Equals(objectCollision.color))
            {
                ScoreSystem.instance.AddScore(100);
            } else
            {
                LifeSystem.instance.LoseLife();
            }

            ColorBehaviour.changeColorFunction();

            ParticleSystemPool.instance.PlayParticle(objectCollision.color,collision.transform.position);
            Destroy(collision.gameObject);
        }
    }
}
