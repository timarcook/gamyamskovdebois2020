﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemPool : MonoBehaviour
{
    public static ParticleSystemPool instance;

    [SerializeField] ParticleController particlePrefab;
    [SerializeField] int particleCount;
    List<ParticleController> controllerList = new List<ParticleController>();

    int index = 0;

    private void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        for(int i = 0; i < particleCount; i++)
        {
            ParticleController pc = Instantiate(particlePrefab);
            pc.transform.parent = this.transform;
            controllerList.Add(pc.GetComponent<ParticleController>());
        }
    }


    public ParticleController getParticle()
    {
        ParticleController pc =  controllerList[index];
        index++;
        index %= particleCount;
        return pc;
    }

    public void PlayParticle(Color color, Vector3 pos)
    {
        getParticle().Init(color, pos);
    }
}