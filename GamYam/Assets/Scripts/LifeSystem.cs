﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LifeSystem : MonoBehaviour
{

    public static LifeSystem instance;

    [SerializeField] RectTransform lifePanelUI;
    [SerializeField] Text scoreOnDeathText;
    [SerializeField] GameObject lifePrefab;
    [Range(1,10)][SerializeField] int maxLives;
    [SerializeField] Canvas deathCanvas;
    [Range(1f, 100f)] public float mellanrum = 50;
    
    int lives = 0;
    List<Image> imageList = new List<Image>();
    List<Animator> animatorList = new List<Animator>();

    [Header("Events")]
    public UnityEvent loseLifeEvent;
    public UnityEvent onDeathEvent;

    void Start()
    {
        Time.timeScale = 1f;
        if(instance == null)
        {
            instance = this;
        }

        else
        {
            Destroy(this);
        }

        lives = maxLives;
        lifePanelUI.sizeDelta = new Vector2(maxLives * mellanrum, lifePanelUI.sizeDelta.y);
        for(int i = 0; i < maxLives; i++)
        {
            GameObject life = Instantiate(lifePrefab);
            RectTransform rectTransform = life.GetComponent<RectTransform>();
            imageList.Add(life.GetComponent<Image>());
            animatorList.Add(life.GetComponent<Animator>());
            rectTransform.parent = lifePanelUI;
            rectTransform.localPosition = new Vector3(i * mellanrum - maxLives * mellanrum + 50, -50, 0);
        }
      
    }

    public int getLives()
    {
        return lives;
    }

    public void LoseLife()
    {
        if(lives >= 0)
        {
            StartCoroutine(disableGameObject(0.3f, Mathf.Abs(lives - maxLives)));
            lives--;

            if (lives <= 0)
            {
                Death();
            }
            else
            {
                loseLifeEvent.Invoke();
            }
        }
    }

    void Death()
    {
        deathCanvas.enabled = true;
        Time.timeScale = 0;
        scoreOnDeathText.text = "FinalScore: " + ScoreSystem.instance.getScore();
        onDeathEvent.Invoke();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator disableGameObject(float time,int index)
    {
        animatorList[index].SetTrigger("loseHealthTrigger");
        yield return new WaitForSeconds(time);
        imageList[index].enabled = false;
    }
}
