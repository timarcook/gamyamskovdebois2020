﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PauseBehaviour : MonoBehaviour
{
    [SerializeField] Canvas PauseCanvas;
    [SerializeField] Image pausedBackground;
    [SerializeField] float fadeTime;
    float t = 0;

    [Header("Events")]
    [SerializeField] UnityEvent onPauseEvent;
    [SerializeField] UnityEvent onUnPauseEvent;

    Color fadeInToColor;
    Color fadeFromColor;

    bool isPaused = false;
    // Start is called before the first frame update
    void Start()
    {
        fadeFromColor = getRandomColor();
        pausedBackground.color = fadeFromColor;
        fadeInToColor = getRandomColor();

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
                Pause();
            else if (isPaused)
                UnPause();
        }

        if(isPaused)
        {
            Fade();
        }
    }


    Color getRandomColor()
    {
        return new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
    }

    void Fade()
    {
        t += Time.unscaledDeltaTime / fadeTime;
        pausedBackground.color = Color.Lerp(fadeFromColor, fadeInToColor, t);
        if(t >= 1)
        {
            fadeFromColor = fadeInToColor;
            fadeInToColor = getRandomColor();
            t = 0;
        }
    }


    private void Pause()
    {
        onPauseEvent.Invoke();
        Time.timeScale = 0;
        isPaused = true;
        PauseCanvas.enabled = true;
    }

    public void UnPause()
    {
        onUnPauseEvent.Invoke();
        Time.timeScale = 1;
        isPaused = false;
        PauseCanvas.enabled = false;
    }

    public void Quit()
    {
        SceneManager.LoadScene(0);
    }
}
